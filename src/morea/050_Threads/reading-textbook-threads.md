---
morea_id: textbook_threads
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: "OSC Chapter 4 (but not Sections 4.4 and 4.5)"
morea_url: "javascript:void(0);"
morea_labels: 
  - "Reading"
---
