---
morea_id: experience_threads
morea_type: experience
title: "Homework Assignment #6"
published: True
morea_summary: "An assignment in which you enhance Jsh with Threads"
morea_labels: 
  - "Due Thu Oct 27 2016 23:55 HST"
---

## Programming Assignment -- Threads / Jsh v2 

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

The due date for this assignment is Thu October 27th 2016, 11:55pm HST.

Check the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>
for the late assignment policy for this course.

<b>This assignment gives a total of 38 points and 2 extra-credits.</b>


#### What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw6_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw6_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw6_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw6_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw6_YourUHUserName directory:

 * index.html: your report
 * src: the source code for the various questions

It should contain at least:

 TODO

---

#### Environment

For this assignment you need a POSIX environment and a Java 8 SDK (see
<a href="{{ site.baseurl }}/morea/010_GettingStarted/experience-gettingstarted.html">HW#1</a>).

---

### Assignment Overview

In this assignment we enhance Jsh so that it prints standard output
and error from commands asynchronously.

-------------------

### Question #1 [5 pts]: FlipFlop

Implement a Java program (let's call it FlipFlop) in the package
edu.hawaii.ics332.fliflop that takes as input two integer command-line
arguments, say <tt>x</tt> and <tt>y</tt>. The program then creates two
threads.

One thread performs 10 iterations consisting of printing the
string "[date] flip\n" to <b>stdout</b> and then sleeping for
<tt>x</tt> milliseconds

The other thread performs 10 iterations consisting of printing the
string "[date] flop\n" to <b>stderr</b> and then sleeping for
<tt>y</tt> milliseconds

 * For [date], use <tt>(System.currentTimeMillis() % 10000)</tt>
   (it gives a truncated time information)

 * "Sleeping" can be performed with the
   <tt>Thread.sleep(sleepingTimeMillis)</tt> method

Run your program for x=2 and y=3. Make sure that it outputs 'flip' 10
times on stdout and 'flop' 10 times on stderr (detail how you make
sure). Copy/paste the output of the program in your report and comment
what you see (especially in terms of unexpected timing).

-------------------

### Question #2 [8 points + 2 extra-credits]: FlipFlopGobbler

Implement a Java program (let's call it FlipFlopGobbler) in the
package edu.hawaii.ics332.fliflopgobbler that takes two integer
command-line arguments (<tt>x</tt> and <tt>y</tt>) and creates an
external process that executes the program that you wrote in question.

FlipFlopGobbler must capture stdout and stderr from FlipFlop and print
them as they are produced (to stdout and to stderr). Consequently, the
sequencing of flip and flop strings should be similar to that observed
when running FlipFlop directly (there may be a few differences in
ordering and carriage returns, but the overall behavior should be
similar).

To do this, you must implement a class called PipeRunnable that
implements the Runnable interface:

 * Its class constructor takes two parameters: one InputStream and one
   OutputStream.

 * Its run() method reads lines from the InputStream and prints them
   to the OutputStream until there is nothing left to be read from the
   InputStream.

Run your program for x=2 and y=3. Verify that the output is similar to
the output of the FlipFlop program. 

Hints:

 * java.io.BufferedReader is convenient to read lines

 * java.io.PrintStream is convenient to print lines

<b>Extra-credit [2 points]:</b> Describe shortly how you would switch
the outputs to stdout and stderr (Which lines of your code would you
modify in FlipFlopGobbler? How would you modify them?)

-------------------------------------

### Question #3 [10 points]: Asynchronous output in Jsh

Using the PipeRunnable class from the previous question, enhance Jsh
(see <a href="/morea/040_Processes/experience-jsh1.html">HW#5</a>,
let's call this new class edu.hawaii.ics332.apps.JshB1 to fix ideas)
so that it prints out stdout and stderr from commands asynchronously,
i.e., as they are generated. See example below:

{% highlight text %}
jsh> java edu.hawaii.ics332.fliflop.FlipFlop 2 3
8075: flip
8075: flop
8077: flip
8078: flop
8079: flip
8081: flop
8081: flip
8083: flip
8084: flop
8086: flip
8087: flop
8088: flip
8090: flip
8091: flop
8092: flip
8094: flop
8094: flip
8097: flop
8100: flop
8103: flop
jsh>
{% endhighlight %}

----------------

### Question #4 [15 points]: Jsh - Background execution

All shells provide a way to execute processes in the background, i.e.,
asynchronously. When a command is typed that ends with a "&", then the
prompt comes back right away but the typed command keeps running.

A shell built-in command called <b>jobs</b> can be used to see all
commands currently running in the background, each associated with a
unique number (a global counter which is incremented each time a new
command is placed in the background).

Another built-in command called <b>fg</b> puts the command with a
given number in the foreground (i.e., the Shell is blocked until the
command terminates). As a result, the <b>fg</b> command may return
right away if the job has already completed.

Trying to put a built-in commands (cd, jobs, fg) in the background is
an error.

In this question, you have to extend
<tt>edu.hawaii.ics332.apps.JshB1</tt> by creating the class
<tt>edu.hawaii.ics332.apps.JshB2</tt> and any class you will find
necessary implementing:

 * a "&" operator, 
 * a <b>jobs</b> built-in, 
 * and a <b>fg</b> built-in

In the report you will first write down the specifications for each of them
as you understand them (Feel free to read the man page associated to
them but do not try to implement the full-flavored).

Then you will then detail (in the report) how you implemented them (referring to your
source code)

Finally you will show and detail (still in the report) the outcome of a few test
cases:

 * one nominal test case e.g. two FlipFlopGobblers running
   concurrently with "large" sleeping time parameters, e.g. 150 200
   for one, and 125 250 for the other. Then while it is executing what
   the jobs built-in displays, what the effect of "fg 2" is

 * error test cases that you can think of: like "jobs &", "fg 12345"...

Here is an example interaction with <b>sleep</b> invocations:
{% highlight text %}
jsh> jobs &
Error: cannot put 'jobs' in the background
jsh> sleep 10 &
[1] sleep 10
jsh> sleep 50 &
[2] sleep 50
jsh> jobs
[1] sleep 10
[2] sleep 50
jsh> fg 1
jsh> jobs
[3] sleep 50
jsh> fg 10
Error: No such background command
jsh> fg 2
jsh> jobs
jsh> ^D
{% endhighlight %}


-------------------

