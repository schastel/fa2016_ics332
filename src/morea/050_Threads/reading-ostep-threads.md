---
morea_id: textbook_threads_ostep1
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: "OSTEP Concurrency and Threads 26.1-2"
morea_url: "http://pages.cs.wisc.edu/~remzi/OSTEP/threads-intro.pdf"
morea_labels: 
  - "Reading"
---
