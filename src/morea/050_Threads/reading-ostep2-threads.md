---
morea_id: textbook_threads_ostep2
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: "OSTEP Thread API 27.1-2"
morea_url: "http://pages.cs.wisc.edu/~remzi/OSTEP/threads-api.pdf"
morea_labels: 
  - "Reading"
---

