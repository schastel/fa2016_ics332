---
morea_id: outcomethreads
morea_type: outcome
title: "Threads"
published: True
morea_sort_order: 30
---

- Understand the thread abstraction and how the OS implements it
- Gain hands-on experience with thread creation in Java


