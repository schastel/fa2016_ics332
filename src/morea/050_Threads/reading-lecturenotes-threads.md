---
morea_id: lecturenotes_threads
morea_type: reading
title: "Threads"
published: True
morea_summary: "Threads, User vs. Kernel Threads,  Java Threads, Threads and OS implementations"
morea_url: /morea/050_Threads/ics332_threads.pdf
morea_labels: 
  - "Lecture notes"
---
