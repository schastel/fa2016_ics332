---
title: "Homework Assignment #1"
published: true
morea_id: experiences-gettingstarted
morea_summary: "An assignment to make sure you have access to
a (virtual) Linux box"
morea_type: experience
morea_sort_order: 0
morea_labels: 
- "Due Thu Sep 8th 2016, 23:55 HST"
---

## Hands-On Assignment: Having access to a Linux system

---
You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

#### What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw1_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw1_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics32_hw1_schastel). Inside that directory you should have all your
other files, <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML (a template is
provided <a href="ics332_hw1_YourUHUserName/index.html">here</a>).

The deadline is <b>firm</b>. Since this HW is extra-credit only no
point will be given for turning it in after the deadline.

---

This assignment gives 0 point and 5 extra-credit points (Homework).

---

<h4>Install a Virtual Machine (3 extra-credit points)</h4>

Assignments in this course require that you use a POSIX-compliant
system. To avoid differences between the expected results and the
results you'll be getting in them, we will ask you to install a
well-defined Linux OS on your computer.

<p/>
The purpose of this assignment is to install a Linux Virtual Machine
on your computer (please do it even if you already use Linux).

<p/>

Carefully follow the following steps:
<ul>
<li>(1.5GB to download) Download the 64-bit Ubuntu 16.04 desktop distribution, which you can get from
the <a
href="http://mirror.ancl.hawaii.edu/linux/ubuntu-releases/16.04.1/">UH
mirror</a> (ubuntu-16.04.1-desktop-amd64.iso) </li>

<li>Install the VirtualBox Virtual Machines Monitor (a.k.a. a hypervisor) (other VMM exist: VMWare,
Xen...). See the instructions at <a
href="http://henricasanova.github.io/VirtualBoxUbuntuHowTo.html">
Installing Ubuntu on VirtualBox</a> page. Make sure to complete all steps, that is:
<ul>
<li>Install Ubuntu (15-20 minutes)</li>
<li>Install VirtualBox additions (5 minutes)</li>
<li>Create a shared folder (1 minute)</li>
</ul></li>
<li>Reboot your VM and log in</li>
<li>In a terminal, execute <tt>cat /proc/cpuinfo | grep processor | wc -l</tt> (it is the number of cpus in your VM)</li>
<li>Shutdown your VM</li>
<li>Boost your VM by changing the number of processors (it's 1 by default): Settings / System / Processor. Use the maximal suggested value (2? 4? more? less?)</li>
<li>Restart your VM and check the number of cpus (<b>Question 1 (1 point)</b>)</li>

<li>Make a (first) clone of your Virtual Machine (Let's call it "Ubuntu Clone Almost Good" to fix ideas). If something (bad) happens in the next steps, that will avoid reinstalling everything from scratch</li>

<li>Softwares for the future
<ul>
<li>Install Java 8 (both the open source and the Oracle versions)</li>
<pre>
sudo apt-get update
sudo apt-get install default-jdk
</pre>
<li>Report the default Java version <t>java -version (<b>Question 2.1 - 0.5 point</b>)</t>
<pre>
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
</pre>
</li>
<li>Report the new Java version <t>java -version</t> (<b>Question 2.2 - 0.5 point</b>)</li>

<li>Install the hwloc package (sudo apt-get install hwloc) and run
lstopo. Use man lstopo to know how to get the picture sent to the PNG
image file names <tt>lstopo.png</tt>. Provide that image in your
report. Compare with the results of Question 1 (<b>Question 3 - 1 point</b>)</li>
</ul>
</li>

<li>Make a clone (a full clone): Let's call it "Ubuntu Installation
Reference Clone". If during the semester, you mess up (or something else
messes up) with VM you'll have a fresh one without having to reinstall
everything. Feel free to delete the "Ubuntu Clone Almost Good" VM.</li>

</ul>

<i>Note: </i>From now on, you know the basics about VM manipulation. Feel free to do your own backups at any time.

<h4>Reporting (2 extra-credit points)</h4>

It is expected that your archive follows the following requirements (1
extra-credit point for each requirement).

<b>Requirement 1:</b> The name of your archive shall be: 
<pre>
ics332_hw1_UHUSERNAME.tar.gz
</pre>
where UHUSERNAME is your UH User Name.

<b>Requirement 2:</b> When extracted your full report shall exactly contain the two files named:
<pre>
ics332_hw1_UHUSERNAME/index.html
ics332_hw1_UHUSERNAME/lstopo.png
</pre>

where UHUSERNAME is your UH User Name (as in 'UHUSERNAME@hawaii.edu')

Notes:
<ul>
<li>Always make a backup of your data when you are not sure of what
you are doing (... and even when you think you know)!  </li>
<li>
To create a tarball gzipped archive: <tt>tar cvfz archive.tar.gz file1 file2 directory1 [...]</tt>
</li>
<li>
To extract from a tarball gzipped archive: <tt>tar xvfz archive.tar.gz</tt>
</li>
</ul>

<p/>
