\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Foreword}{2}{0}{1}
\beamer@sectionintoc {2}{Basics}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{Terminal. CLI. Shell}{3}{0}{2}
\beamer@subsectionintoc {2}{2}{Shell}{4}{0}{2}
\beamer@subsectionintoc {2}{3}{Commands}{5}{0}{2}
\beamer@subsectionintoc {2}{4}{Pipes}{8}{0}{2}
\beamer@sectionintoc {3}{Miscellaneous}{10}{0}{3}
\beamer@subsectionintoc {3}{1}{Jobs Management}{10}{0}{3}
\beamer@subsectionintoc {3}{2}{Environment Variables}{11}{0}{3}
\beamer@subsectionintoc {3}{3}{Shell Customization}{12}{0}{3}
\beamer@subsectionintoc {3}{4}{Tricks and Tips}{13}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{15}{0}{4}
\beamer@subsectionintoc {4}{1}{Limits. Conclusion}{15}{0}{4}
