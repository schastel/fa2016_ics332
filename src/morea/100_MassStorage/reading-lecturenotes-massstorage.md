---
morea_id: lecturenotes_massstorage
morea_type: reading
title: "Mass Storage"
published: True
morea_summary: "Hard Drives, Disk Scheduling, RAID, SSDs"
morea_url: /morea/100_MassStorage/ics332_massstorage.pdf
morea_labels: 
  - "Lecture notes"
---
