---
morea_id: 100_MassStorage
morea_type: module
title: "Mass Storage"
published: True
morea_coming_soon: False
morea_highlight: False
morea_outcomes: 
  - outcomefilesystems
morea_readings: 
  - lecturenotes_massstorage
  - textbook_massstorage
morea_experiences: 
morea_assessments: 
morea_sort_order: 120
morea_icon_url: /morea/100_MassStorage/logo.jpg
---

