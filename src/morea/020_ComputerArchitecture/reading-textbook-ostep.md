---
morea_id: textbook_computerarchitecture_ostep
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: "OSTEP: Introduction"
morea_url: "http://pages.cs.wisc.edu/~remzi/OSTEP/intro.pdf"
morea_labels: 
  - "Reading"
---
