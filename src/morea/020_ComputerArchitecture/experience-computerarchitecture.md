---
morea_id: experience_computerarchitecture
morea_type: experience
title: "Homework Assignment #2"
published: True
morea_summary: "A pencil-and-paper assignment to make sure that you understand the fetch-decode-execute cycle, and that you know about caches"
morea_labels: 
  - "Due Thu Sept 15th 2016, 11:55pm HST"
---

## HW#2 Pencil-and-Paper Assignment -- Computer Architecture

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work. (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>.)

Check the <a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a> for
the late assignment policy for this course.

#### What to turn in?

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw2_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw2_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics32_hw2_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw2_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw2_YourUHUserName directory:

 * index.html: report

---

### Exercise #1: The Fetch-Decode-Execute Cycle [9 points + 1 extra-credit]

The purpose of this exercise is to test the understanding of the
Fetch-Decode-Execute cycle.

Consider a fictitious and very simple Von-Neumann architecture with

 * an 8-bit addresses/12-bit values memory
 * 2 12-bit registers for computation named A and B;
 * 1 8-bit Program Counter register;
 * 1 12-bit Current Instruction register;
 * 6 instructions: LOAD, STORE, ADD, SUBTRACT, JNZ (Jump if not zero), and STOP

Each instruction is encoded with 12 bits: 

 * the first 3 bits give the operation code (opcode) to identify the
instruction,
 * the next bit give the number of the register that the instruction
operates with (0 addressing A, 1 addressing B), and
 * the last 8 bits are an operand. When not ignored, that can be an
address, an 8-bit positive integer number, depending on the
instruction.

<table border="1">
 <thead><tr><th></th><th>Instruction</th><th>Opcode</th><th>Description</th></tr></thead>
 <tbody>
 <tr><td></td><td> LOAD </td><td> 010 </td><td> Load register value with value from address (operand) </td></tr>
 <tr><td></td><td> STORE </td><td> 011 </td><td> Store register value to address (operand) </td></tr>
 <tr><td></td><td> ADD </td><td> 100 </td><td> Add register (last bit of operand the other bits are meaningless) value to register </td></tr>
 <tr><td></td><td> SUB </td><td> 101 </td><td> Substract (operand) value to register </td></tr>
 <tr><td></td><td> JNZ </td><td> 110 </td><td> Set PC value to [operand] if register value is not zero </td></tr>
 <tr><td></td><td> STOP </td><td> 111 </td><td> Terminates program </td></tr>
 </tbody>
</table>

For instance:

* 010110101010 should be read as 010 1 10101010, that is 010 = LOAD, 1
  = B, with the value at address (10101010);

* <font color="blue">100</font><font color="red">0</font><font color="black">0101010</font><font color="green">1</font> should be read as <font color="blue">100</font> (ADD) to register <font color="red">0</font>
  (i.e. register A) the value of register <font color="green">1</font> (that is B because
  0101010<b><font color="green">1</font></b> identifies B): A is updated with the result. If
  initially the value of A was 100000000101 and the value of B
  010000000010, at the end of the operation, the value of A would be
  110000000111 (and B is 010000000010 (unchanged)).

* 110100001111: Set the Program Counter value to 00001111 if B is not
  0

Assume that initially:

* The Program Counter value is set to 00000100; The values of the
  other registers are undefined.
 
* The memory has the following contents:

<table border="1">
 <thead><tr><th>Address</th><th>Value</th></tr></thead>
 <tbody>
  <tr><td>00000000 </td><td> 000001100110</td></tr>
  <tr><td>00000001 </td><td> 000011000011</td></tr>
  <tr><td>00000010 </td><td> 000001001000</td></tr>
  <tr><td>00000011 </td><td> 000010001110</td></tr>
  <tr><td>00000100 </td><td> 010010000100 </td></tr>
  <tr><td>00000101 </td><td> 010110000101 </td></tr>
  <tr><td>00000110 </td><td> 100000000000 </td></tr>
  <tr><td>00000111 </td><td> 101100000001 </td></tr>
  <tr><td>00001000 </td><td> 110100000110 </td></tr>
  <tr><td>00001001 </td><td> 011000000000 </td></tr>
  <tr><td>00001010 </td><td> 011110101100 </td></tr>
  <tr><td>00001011 </td><td> 111001110110 </td></tr>
  <tr><td>00001100 </td><td> 110110101000 </td></tr>
  <tr><td>00001101 </td><td> 111111110001 </td></tr>
  <tr><td>  ...    </td><td>      ...     </td></tr>
  <tr><td>10000000 </td><td> 000000111000</td></tr>
  <tr><td>10000001 </td><td> 000011100001</td></tr>
  <tr><td>10000010 </td><td> 000001000000</td></tr>
  <tr><td>10000011 </td><td> 000000011111</td></tr>
  <tr><td>10000100 </td><td> 000000000001</td></tr>
  <tr><td>10000101 </td><td> 000000000011</td></tr>
  <tr><td>10000110 </td><td> 000000101011</td></tr>
  <tr><td>10000111 </td><td> 000011100010</td></tr>
  <tr><td>  ...    </td><td>      ...     </td></tr>
 </tbody>
</table>

<b>Question 1 (1 point)</b>: What is the first instruction executed by the
program? Explain.

<b>Question 2 (5 points)</b>: Detail what each stage of the
Fetch-Decode-Execute cycle does for each instruction executed until
the first JNZ instruction is encountered (including this first JNZ).

Explain what each stage in the cycle does and how the instruction
modifies the values of the various components in the system.

<b>Question 3 (3 points)</b>: What are the values of the registers
and the memory where it has been modified when the program terminates
(if it does terminate)? Explain.

<b>Question 4 (1 extra-credit)</b>: What does this program compute? Explain

### Exercise #2: Caches [2 pts]

Answer these questions (0.5 point each):

 * What is the goal of having caches in a computer?

 * How come caches work?

 * What is a problem caused by caches?

 * Why not make a cache as large as large as the device for which it
   is caching (for instance, a cache as large as the main memory disk)
   and simply eliminate the device altogether?
