---
morea_id: outcomearch
morea_type: outcome
title: "Computer Architecture"
published: True
morea_sort_order: 5
---

- Understand the main components of the Von-Neumann model
- Understand the concept of an Instruction Set Architecture
- Understand the Fetch-Decode-Execute cycle

