---
morea_id: textbook_computerarchitecture_osg
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: "OSC: Sections 1.1, 1.2, and 1.3"
morea_url: "javascript:void(0);"
morea_labels: 
  - "Reading"
---
