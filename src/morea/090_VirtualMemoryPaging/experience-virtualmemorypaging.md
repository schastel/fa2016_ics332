---
morea_id: experience_virtualmemorypaging
morea_type: experience
title: "Homework Assignment #9"
published: True
morea_summary: "A pencil-and-paper assignment to make sure that you understand page tables"
morea_labels: 
  - "Due December 8th 2016 - 23:55 HST"
---

## Pencil-and-Paper Assignment -- Page Tables

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

The due date for this assignment is Thu December 8th 2016, 11:55pm HST.

Check the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>
for the late assignment policy for this course.

<b>This assignment gives a total of 18 points and 3 extra-credits.</b>

---

#### What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw9_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw9_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw9_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw9_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw9_YourUHUserName directory:

 * index.html: your report

---

### Exercise #1: Page Table [8 points + 2 extra-credits]

Consider the following page table, in which "x" means an invalid entry.

{% highlight text %}
------------------
logical | physical 
------------------
|  15   |   9    |
|  14   |   x    |
|  13   |   40   |
|  12   |   x    |
|  11   |   25   |
|  10   |   x    |
|  9    |   x    |
|  8    |   7    |
|  7    |   x    |
|  6    |   5    |
|  5    |   x    |
|  4    |   8    |
|  3    |   x    |
|  2    |   1    |
|  1    |   0    |
|  0    |   11   |
------------------
{% endhighlight %}
 
Assume a 1KiB (i.e., 1,024 bytes) page size.  Give the physical address
corresponding to the following three logical addresses. <b>For convenience,
in this exercise all physical and logical addresses are integer values.</b>
Your answer should thus be decimal values and should include the details
of how you performed the computation. 

 - <b>a. [2pts]</b> 2020
 - <b>b. [2pts]</b> 8205
 - <b>c. [2pts]</b> 6150
 - <b>d. [2pts]</b> 13963
 - <b>3. [2pts EXTRA CREDIT]</b> Give a logical address (in the range 0-32767) that
will generate a page fault given the above page table and explain.

-------------------

###  Exercise #2: Logical and physical address [2 points]

Consider a logical address space of 64 pages with 2,048 bytes per page,
mapped onto a physical memory of 16 frames.

  - <b>a. [1pts]</b> How many bits are required in the logical address?
  - <b>b. [1pts]</b> How many bits are required in the physical address?<br>


--------------------

### Exercise #3: Page table size [4 points]

Consider a computer system with a 40-bit logical address space and
8-KiB page size. The system supports up to 2GiB of physical memory.
(Give all answers as powers of 2.)

  - <b>a. [2pts]</b> How many entries are there in a conventional
    single-level page table? (show your work)

  - <b>b. [2pts]</b> How many entries are there in an inverted page
    table? (show your work)


---------------

### Exercise #4: Page table [4 points + 1 extra-credit]

Suppose we have a computer system with a 38-bit logical address, page
size of 32KiB, and 8 bytes per page table entry.

  - <b>a. [4pts]</b> Suppose we use two-level paging and arrange for
all page tables to fit into a single page frame. How will the bits of
the address be divided up? Is the outer page table page fully
utilized? (show your work)

  - <b>b. [1pts EXTRA CREDIT]</b> In the scheme above, if only 7 of
    the entries in the outer-level page table page are valid, and only
    1/4 of the entries in all the inner-level page table pages are
    valid, what is the size of the address space in MiB?


---------------
