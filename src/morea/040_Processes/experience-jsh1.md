---
morea_id: experience_java_and_processes
morea_type: experience
title: "Homework Assignment #5"
published: True
morea_summary: "An assignment in which you implement a Shell in Java"
morea_labels: 
  - "Due Thu Oct 13th 2016, 11:55pm HST"
---

## Programming Assignment -- Jsh v1

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

The due date for this assignment is Thu October 13th 2016, 11:55pm HST.

Check the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>
for the late assignment policy for this course.

<b>This assignement gives a total of 30 points and 0 extra-credit.</b>

#### What to turn in?

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw5_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw5_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw5_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw5_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw5_YourUHUserName directory:

 * index.html: your report
 * src: the source code for the various questions

 It should contain at least:

    src/edu/hawaii/ics332/apps/JshA1.java
    
    src/edu/hawaii/ics332/apps/JshA2.java
    
    src/edu/hawaii/ics332/apps/JshA3.java
    
 But you are free to factorize the code for future reuse and any class
 that you think useful. All source code must be documented using
 Javadoc. All classes must be referenced in your report with a short
 description of their purpose.

---

#### Environment

For this assignment you need a POSIX environment and a Java 8 SDK (see
<a href="{{ site.baseurl }}/morea/010_GettingStarted/experience-gettingstarted.html">HW#1</a>).

In this assignment you will develop a simple Shell, implemented in
Java, and that will be called <tt>Jsh</tt>. Jsh provides a small
subset of the functionality of a real Shell (ksh, bash, csh, etc.).
You will extend Jsh in subsequent assignments.

To help you with unrelated code that proves - from experience - to be sometimes
tricky (and that does not target what we'd like  you to learn), a jar of tools is provided: you can download it <a
href="lib/jshCommon.jar">here</a>. Its contents will be detailed when necessary.

To compile Java code on the CLI, have a look <a href="java.html">this</a>.

#### Requirements for the whole project

The base directory for all your source code will be named src, i.e. the
source file associated to the class edu.hawaii.ics332.apps.MyTest will
be src/edu/hawaii/ics332/apps/MyTest.java

The base directory for any compiled code will be named bin, i.e. the
class file associated to the class edu.hawaii.ics332.apps.MyTest will
be bin/edu/hawaii/ics332/apps/MyTest.class

You are required to deliver source code which compiles without error (source
code that does not compiled will not be reviewed).

You are required to deliver readable source code (commented, not
obfuscated).

You are expected to deliver source code that pass all the tests. If
the tests do not pass, you can still provide the source (it has to
compile though) and explain why you think it does not work in your
report. That does not guarantee that you will get any point for the
question though.

"Good software practices" are encouraged, e.g. code reuse, no
copy/paste...

### Running simple commands [20 points]

The purpose of this question is to implement Jsh so that it prints a
prompt and accepts and executes user commands.

<b>Question 1: Prompt, command clean-up [10 points]</b>

Implement the class <tt>edu.hawaii.ics332.apps.JshA1</tt> so that:

 (1) It prints a prompt looking like <tt>jsh> </tt>
 
 (2) It accepts anything a user would enter and removes all sequence
 of multiple whitespaces (that is, spaces, tabs) by a unique space character.

 To help you with the replacement, feel free to use
 <tt>Tools.cleanUp(String)</tt> from the provided <a href="lib/jshCommon.jar">jshCommon.jar</a>

 (3) When the user enters only whitespaces or an empty string,
 the prompt should be presented again, e.g.:
<pre>
jsh>&nbsp;<font color="red">&nbsp;</font><font color="blue">&lt;TAB&gt;</font><font color="green">&lt;CR&gt;</font>
jsh> 
</pre>

 (4) For testing purposes, in this question only, display the filtered
 user entry on the standard output (no validation of what has been entered is expected), e.g.:
<pre>
jsh>&nbsp;<font color="red">&nbsp;Hello,</font><font color="blue">&lt;TAB&gt;</font>World!<font color="blue">&lt;TAB&gt;</font><font color="green">&lt;CR&gt;</font>
Hello, World!
jsh> The answer is 42<font color="green">&lt;CR&gt;</font>
The answer is 42
jsh> CTRL-D
</pre>

Testing: Assuming that you followed the previous instructions, you
should get (up to the whitespaces) the contents of <a
href="data/jsh1.ref">this reference file</a> when the inputs are those
from <a href="data/jsh1.in">this scenario</a>.

Report: In your report you will give any detail you think useful about
the implementation as well as a copy of the test report (use the
format (text, image) you see fit as soon as it can be integrated to
HTML and read by a standard browser. No CSS/javascript). Write down
what happens when CTRL-D (or ^D) i.e. when pressing the CTRL key (and
keeping it pressed) and the D key).

<b>Question 2: Command execution [10 points]</b>

Implement the class <tt>edu.hawaii.ics332.apps.JshA2</tt> so that:

When the user types a command, Jsh should run that command via the
<tt>ProcessBuilder</tt> class.

The standard error and output streams must be captured and displayed
to the screen.

It is expected that when entering the exit command or hitting CTRL-D,
Jsh exits cleanly.

It is not expected (for now) that interactive commands succeed.

It is not expected that built-ins and aliases succeed.

For instance:
<pre>
bash-prompt$ java -cp lib/jshCommon.jar:bin edu.hawaii.ics332.apps.JshA2
jsh> mkdir -p test
jsh> cd test
cd: command not found
jsh> touch test/a_file
jsh> touch test/another_file
jsh> ls -1 test
a_file
another_file
jsh> ls -1 test/*
test/a_file
test/another_file
jsh> rmdir test
rmdir: failed to remove 'test/': Directory not empty
jsh> rm test/a*
jsh> rmdir test
jsh> Is_this_a_valid_command?
Is_this_a_valid_command?: command not found
jsh> bash
jsh> exit
bash-prompt$ 
</pre>

<i>Hint:</i> The <tt>split()</tt> method of the <tt>String</tt> class
is convenient to parse the command line.

Testing: You should get (up to the whitespaces) the contents of <a
href="data/jsh2.ref">this reference file</a> when the inputs are those
from <a href="data/jsh2.in">this scenario</a>.

Report: In your report you will give any detail you think useful about
the implementation as well as a copy of the test report (use the
format (text, image) you see fit as soon as it can be integrated to
HTML and read by a standard browser. No CSS/javascript)

### Changing directory and maintaining the working directory [10 points]

<b>Question #3:</b>

Implement the class <tt>edu.hawaii.ics332.apps.JshA3</tt> so that:

Enhance Jsh so that it implements the <tt>cd</tt> built-in.

This requires keeping track of the working directory (so that when Jsh
runs a command via <tt>ProcessBuilder</tt> it can specify the working
directory for the command).

If the user simply types <tt>cd</tt>, then the current directory is
changed to the working directory at the time Jsh was started (which
Jsh must therefore "remember").

Finally, <tt>cd</tt> should print appropriate error messages.

Here is an example interaction with the user:
<pre>
jsh> pwd &crarr;
/home/schastel/workspace.ics332/Jsh1 # ... A random directory is displayed here
jsh> mkdir -p test
jsh> cd test
jsh> pwd 
/home/schastel/workspace.ics332/Jsh1/test
jsh> ls 
jsh> touch a_file
jsh> cd ..
jsh> pwd 
/home/schastel/workspace.ics332/Jsh1
jsh> ls test
a_file
jsh> cd test
jsh> mkdir -p 1/2/3/4
jsh> cd 1/2/3/4
jsh> pwd 
/home/schastel/workspace.ics332/Jsh1/1/2/3/4
jsh> cd
jsh> pwd 
/home/schastel/workspace.ics332/Jsh1
jsh> cd 1/2/3/4
jsh> cd ..
jsh> ls
4
jsh> rmdir 4
jsh> ls
jsh> cd ..
jsh> rmdir 3
jsh> cd ..
jsh> rmdir 2
jsh> cd ..
jsh> ls -1
1
a_file
jsh> rmdir 1
jsh> rm a_file
jsh> ls test
jsh> cd ..
jsh> rmdir test
jsh> touch there_should_not_be_a_file_named_like_this
jsh> cd there_should_not_be_a_file_named_like_this
jsh: cd: there_should_not_be_a_file_named_like_this: Not a directory
jsh> rm there_should_not_be_a_file_named_like_this
jsh> exit
</pre>

<i>Hint:</i> The <tt>File.getCanonicalPath()</tt> method is very
convenient to deal with things like "cd
./././dir3/../dir2/../dir1//////../somedir", which is really "cd
somedir"). The Path.resolve(String ... dirs) works fine too.

<i>Hint:</i> To get the working directory of Jsh you can either ask
for its value with System.getProperty("user.dir") and create a
java.io.File, or request it using java.nio.file.Paths.get("")

Testing: You should get (up to the whitespaces) the contents of <a
href="data/jsh3.ref">this reference file</a> when the inputs are those
from <a href="data/jsh3.in">this scenario</a>.

Report: In your report you will give any detail you think useful about
the implementation as well as a copy of the test report (use the
format (text, image) you see fit as soon as it can be integrated to
HTML and read by a standard browser. No CSS/javascript)
