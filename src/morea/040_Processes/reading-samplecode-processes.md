---
morea_id: samplecode_processes
morea_type: reading
title: "Sample code"
published: True
morea_summary: ""
morea_labels: 
  - "code"
---

#### Sample programs discussed in lecture notes

- Linux processes:
  - [fork_example1.c](examples/fork_example1.c)
  - [fork_example2.c](examples/fork_example2.c)
  - [fork_example3.c](examples/fork_example3.c)
  - [fork_example4.c](examples/fork_example4.c)
  - [exec_example1.c](examples/exec_example1.c)
  - [exec_example2.c](examples/exec_example2.c)
  - [exec_example3.c](examples/exec_example3.c)
  - [exec_example4.c](examples/exec_example4.c)
  - [wait_example1.c](examples/wait_example1.c)
  - [wait_example2.c](examples/wait_example2.c)
  - [wait_example3.c](examples/wait_example3.c)
  - [wait_example4.c](examples/wait_example4.c)
  - [orphan_example1.c](examples/orphan_example1.c)
  - [orphan_example2.c](examples/orphan_example2.c)
  - [signal_example.c](examples/signal_example.c)

- IPCs:
  - [posix_shm_example.c](examples/posix_shm_example.c)
