---
morea_id: lecturenotes_ipcs
morea_type: reading
title: "IPCs"
published: True
morea_summary: "Message passing, RPC/RMI, Pipes, Java.ProcessBuilder, Signals"
morea_url: /morea/040_Processes/ics332_ipcs.pdf
morea_labels: 
  - "Lecture notes"
---
