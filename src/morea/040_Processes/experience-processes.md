---
morea_id: experience_processes
morea_type: experience
title: "Homework Assignment #4"
published: True
morea_summary: "A pencil-and-paper assignment to make sure that you understand the fork() system call"
morea_labels: 
  - "Due Thu Oct. 6th 2016, 11:55pm HST"

---

## Pencil-and-Paper Assignment -- Processes and fork()

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

The due date for this assignment is Thu Oct. 6th 2016, 11:55pm HST.

Check the 
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a> 
for the late assignment policy for this course.

<b>This assignement gives a total of 11 points and 1 extra-credit.</b>

#### What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw4_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw4_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw4_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw4_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw4_YourUHUserName directory:

 * index.html: your report

---

### Exercise [11 points + 1 extra-credit]

Two source code looking like C source code are shown. The purpose of
this exercise is not to test you on your C knowledge, it is therefore
assumed that:

 * The two programs can be executed without any issue in a UNIX-like 
   environment

 * It is assumed that a call to <tt>fork()</tt> never fails. The value
   return by <tt>fork</tt> is 0 in the child process, and the PID of the
   child in the parent.

 * The <tt>wait(&status)</tt> function waits for the termination of
   one of the children created by <tt>fork()</tt> and sets the value
   of the status variable to the value of the exit status of that
   child. <tt>wait()</tt> is blocking, that is, it waits for one child
   to finish. If it is called in the case that <tt>fork()</tt> has not
   been called, it waits indefinitely.

#### Part 1

Consider the following C-like program: 

{% highlight C %}
int main() {
  int i = -1;
  for (i=0;i<3;i++) {
    fork();
  }
  exit(i);
}
{% endhighlight %}

<b>Question 1 (2 points):</b> How many processes does the initial process create when
this program is executed? Explain.

<b>Question 2 (2 points):</b> What are the different exit statuses of the processes
created by this program? Count how many processes exit with the
different exit statuses. Explain.

#### Part 2

Consider the following C-like program: 

{% highlight C %}
int id;

int my_fork() {
  int save_id = id;
  int v = fork();
  if (v == 0) {
    id = save_id+1;
  } else {
    int status;
    wait(&status);
    id = 2*status; /* or id = 2*WEXITSTATUS(status) in "real" C */
  } 
  return id;
}

void my_exit(int i) {
  printf("Terminating with status %d\n", i);
  exit(i);
}

int main() {
  int i = -1;
  id = 100;
  for (i=0;i<3;i++) {
    if (i % 2 == 0) { /* % is the modulus operator */
      my_fork();
    }
  }
  my_exit(id);
}
{% endhighlight %}

<b>Question 3 (2 points):</b> Give an output of this program.

<b>Question 4 (5 points):</b> How many processes are created when this
program is executed? What are the different exit statuses of the
processes created by this program? Count how many processes exit with
the different exit statuses. Explain.

<b>Question 5 (1 extra-credit):</b> Is this program deterministic (i.e. can
its execution depend on the order the OS runs the processes)?
Explain.
