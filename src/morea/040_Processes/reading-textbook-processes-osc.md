---
morea_id: textbook_processes_osc
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: "Sections 3.1, 3.2.3, and 3.3"
morea_url: "javascript:void(0);"
morea_labels: 
  - "Reading"
---
