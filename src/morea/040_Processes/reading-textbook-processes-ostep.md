---
morea_id: textbook_processes_ostep
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: "OSTEP"
morea_labels: 
  - "Reading"
---

[The Process](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-intro.pdf)

[The Processi API](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-api.pdf)

[Mechanism: Limited Direct Execution](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-mechanisms.pdf)

