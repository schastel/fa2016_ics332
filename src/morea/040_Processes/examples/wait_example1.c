#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  pid_t pidChild1, pidChild2;
  int exitStatus = 0;

  if ((pidChild1 = fork()) < 0) {
    fprintf(stderr,"Error: can't fork a process. Aborting\n");
    exitStatus = -1;
  } else if (pidChild1 == 0) {
    // In first child
    sleep(5);
    exitStatus = 1;
  } else {
    // In parent
    if ( (pidChild2 = fork()) < 0 ) {
      fprintf(stderr,"Error: can't fork a process. Aborting\n");
      exitStatus = -2;
    } else if (pidChild2 == 0) {
      // In second child
      sleep(3);
      exitStatus = 2;
    } else {
      // In parent
      printf("PID of child1: %d\n", pidChild1);
      printf("PID of child2: %d\n", pidChild2);
      int returnStatus;
      pid_t returnedPID = wait(&returnStatus);
      printf("The first descendant to complete was: %d with return status %d (%d)\n",
	     returnedPID, WEXITSTATUS(returnStatus), returnStatus);
      returnedPID = wait(&returnStatus);
      printf("The second descendant to complete was: %d with return status %d (%d)\n",
	     returnedPID, WEXITSTATUS(returnStatus), returnStatus);
    }
  }

  printf("%d done with exit status: %d\n", getpid(), exitStatus);
  exit(exitStatus); // <=> return 0
}

