#include <unistd.h>
#include <stdio.h>

int main(int argc, char **argv) {
  char* const arguments[] = {"ls", "-l", "/tmp", NULL};
  execv("/bin/ls", arguments);
  printf("This never gets executed...\n");
  return 0;
}

