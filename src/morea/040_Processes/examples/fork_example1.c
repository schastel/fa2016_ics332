#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  pid_t pid;

  pid = fork();


  if (pid < 0) {
    fprintf(stderr,"Error: can't fork a process\n");
    perror("fork()");
    exit(1);
  } else if (pid) { // I am the parent
    fprintf(stdout,"I am the parent and my child has pid %d\n",pid);
    while(1);  // infinite loop
  } else {
    fprintf(stdout,"I am the child and my pid is %d\n",getpid());
    while (1) ; // infinite loop
  }
}

