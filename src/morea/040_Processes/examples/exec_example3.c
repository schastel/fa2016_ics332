#include <unistd.h>
#include <stdio.h>

#include <errno.h>
#include <string.h>

int main(int argc, char **argv) {
  char* const arguments[] = {"ls", "-l", "/tmp", NULL};
  if (execv("/tmp", arguments) == -1) {
    fprintf(stderr, "Error: %s\n", strerror(errno));
  }
  return 0;
}

