---
morea_id: textbook_filesystem
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: ""
morea_labels: 
  - "Reading"
---

OSC:

* Part 1: Sections 10.1-4, 10.6, 10.7
* Part 2: Sections 11.1-8, 11.10

OSTEP:

* Chapter 36: I/O Devices
* Chapter 37: HDD
* Chapter 38: RAID
* Chapter 39: Files and Directories
* <a href="http://pages.cs.wisc.edu/~remzi/OSTEP/file-ssd.pdf">Flash-based SSDs</a>

And if you want to go into the details:

* Chapter 40: FS Implementation
* Chapter 42: FSCK and Journaling
* Chapter 47: Distributed Systems
* Chapter 48: NFS

