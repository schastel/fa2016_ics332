---
morea_id: textbook_synchronization
morea_type: reading
title: "Textbook Readings - Synchronization"
published: True
morea_summary: "Locks; Conditional Variables; Semaphores"
morea_labels: 
  - "Reading"
---

<ul>
<li>OSC: Sections 5.1-2, 5.5, 5.6.4, 5.7.1, 5.9
</li>
<li>
OSTEP:
<ul><li> 
<a href="http://pages.cs.wisc.edu/~remzi/OSTEP/threads-locks.pdf">Locks</a> - 28.1 to 28.8 and then 28.12 to 28.17
</li><li>
<a href="http://pages.cs.wisc.edu/~remzi/OSTEP/threads-locks-usage.pdf">Lock-based Concurrent Data Structures</a> - 29.1
</li><li>

<a href="http://pages.cs.wisc.edu/~remzi/OSTEP/threads-cv.pdf">Condition Variables</a> - 30.1
</li><li>

<a href="http://pages.cs.wisc.edu/~remzi/OSTEP/threads-sema.pdf">Semaphores</a> - 31.1-3
</li></ul>

Note: If you think of taking ICS-432 read the integrality of these
chapters... This should give you a decent head-start. (Reading the whole chapters is never a bad idea anyway...)
</li>
</ul>
