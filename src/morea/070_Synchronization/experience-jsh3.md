---
morea_id: experience_jsh3
morea_type: experience
title: "Homework Assignment #8"
published: True
morea_summary: "An assignment in which you enhance Jsh with a pipe operator"
morea_labels: 
  - "Due Thu Dec 1st 2016, 23:55 HST"
---

## Programming Assignment -- Jsh v3

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

The due date for this assignment is Thu October 27th 2016, 11:55pm HST.

Check the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>
for the late assignment policy for this course.

<b>This assignment gives a total of 15 points and 5 extra-credits.</b>


#### What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw8_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw8_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw8_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw8_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw8_YourUHUserName directory:

 * index.html: your report
 * src: the source code for the various questions

---

#### Environment

For this assignment you need a POSIX environment and a Java 8 SDK (see
<a href="{{ site.baseurl }}/morea/010_GettingStarted/experience-gettingstarted.html">HW#1</a>).

---

### Assignment Overview

In this assignment we enhance Jsh so that it has a |  (a.k.a. pipe) operator
asynchronously.

-------------------

### The "|" Shell Operator [15 points + 5 extra-credits]

Using the PipeRunnable class from the previous homework, enhance Jsh
so that it implements the pipe ('|') operator. Implementing this
operator requires that your PipeRunnable also deal with stdin since
now a command standard output is passed to the standard input of
another command.

<b>[Extra-Credit: 5 points]</b> You'll receive 5 points extra credit if your Jsh
allows for multiple consecutive pipes.

Here is an example interaction with the user, assuming that
you have completed the extra-credit portion:

{% highlight text %}
jsh> cd /tmp 
jsh> pwd
/tmp
jsh> ls
hsperfdata_zg
keyring-dtU7cU
orbit-zg
pulse-3JzlWAZJQl30
ssh-yIUSXM1075
virtual-zg.vRR5yL
jsh> ls | grep zg | sed s/zg/X
hsperfdata_X
orbit-X
virtual-X.vRR5yL
jsh> cat -h hsperfdata_zg | grep zg
cat: invalid option -- 'h'
Try `cat --help' for more information.
jsh> ls | grep -t zg
grep: invalid option -- 't'
Usage: grep [OPTION]... PATTERN [FILE]...
Try `grep --help' for more information.
jsh> ls /tmp | car | grep zg
Unknown command car
jsh> cd /home/zg/ics332 
jsh> <b>java FlipFlop 40 10 | wc -l
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
flop
20
jsh>
{% endhighlight %}

You are free to implement whatever semantic you want for "empty pipes"
(e.g., "||", "ls|", "|ls", "cd .. | ls", "ls | cd"), however your code
must not crash for these cases.

<b>Two important things:</b>
  
  - In many cases, your PipeRunnables will have to close their
       OutputStream, so that another PipeRunnable gets an EOF on their
       InputStream (since PipeRunnable send data between each other via
       streams). However, in some cases you do not want to to this,
       e.g., you never want to close System.out or System.err. So you
       may have to modify your implementation of PipeRunnable from the
       previous question to allow optional closing of the
       OutputStream.

  - Your code should not lead to left-over threads (i.e.,
    PipeRunnables). We will check on this when we test your code.


-------------------------------
