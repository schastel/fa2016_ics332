---
morea_id: textbook_synchronization_osc
morea_type: reading
title: "Textbook Reading - OSC"
published: True
morea_summary: "Sections 5.1-2, 5.5, 5.6.4, 5.7.1, 5.9"
morea_url: "javascript:void(0);"
morea_labels: 
  - "Reading"
---
