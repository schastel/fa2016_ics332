---
morea_id: lecturenotes_conbugs
morea_type: reading
title: "Common Concurrency Bugs"
published: True
morea_summary: "The deadlock problem, deadlock resolution"
morea_url: /morea/070_Synchronization/conbugs.pdf
morea_labels: 
  - "Lecture notes"
---
