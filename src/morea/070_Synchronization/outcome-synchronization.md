---
morea_id: outcomesynchronization
morea_type: outcome
title: "Synchronization"
published: True
morea_sort_order: 40
---

- Understand the race condition problem
- Understand the lock abstraction 
- Understand the deadlock problem and some of its possible solutions
- Be exposed to the condition variable abstraction


