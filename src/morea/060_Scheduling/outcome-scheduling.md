---
morea_id: outcomescheduling
morea_type: outcome
title: "Scheduling"
published: True
morea_sort_order: 50
---

- Understand the need for scheduling at the OS level
- Understand how scheduling impacts system performance and reactivity
- Know standard scheduling algorithms/strategies and be able to apply them
  to job mixes
- Gain hands-on experience implementing a Scheduling simulation in Java



