---
morea_id: experience_scheduling
morea_type: experience
title: "Homework Assignment #7"
published: True
morea_summary: "A pencil-and-paper assignment to make sure that you understand scheduling"
morea_labels: 
  - "Due Thu Nov 17th 2016 23:55 HST"
---

## Pencil-and-Paper Assignment -- Scheduling

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

The due date for this assignment is Sunday, Nov. 21th 2016, 23:55 HST.

Check the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>
for the late assignment policy for this course.

<b>This assignment gives a total of 26 extra-credits.</b>


### What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw7_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw7_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw7_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw7_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw7_YourUHUserName directory:

 * index.html: your report

### Assignment Overview

The purpose of the problem is to evaluate various metrics for
different scheduling algorithms, see how these metrics are influenced
by context-switching, and then observe the influence of a serial I/O
device on processing.

<h2>Foreword</h2>

Any task can be described by:

<ul>
  <li>Its name, typically a string describing what it does,
  e.g. web_browser </li>
  <li>An arrival date, e.g. 20 (the time unit doesn't really matter,
  the idea is just to be able to identify if one task happens before
  an other)</li>
  <li>A series of descriptors representing the sequence of CPU-bursts
    and I/O-bursts for the task
    <ul>
      <li>CPU-bursts start with the letter c followed by an integer
        representing the duration of the burst, e.g. <i>c30</i> means
        that there is CPU-burst lasting 30 time units</li>
      <li>I/O-bursts (e.g. reading the contents of a file to memory,
        waiting for user input) start with the letter i followed by an
        integer representing the duration of the burst,
        e.g. <i>i40</i> means that there is an IO-burst lasting 40
        time units</li>
    </ul>
  </li>
  <li>The same time unit is used for both the arrival times and the
  durations</li>
</ul>

The purpose of the problem is to evaluate various metrics for
different scheduling algorithms, see how these metrics are influenced
by context-switching, and then observe the influence of a serial I/O
device on processing.
<p/>

You will show your work by displaying the sequence of events happening
on the CPU along the timeline. You are free to use any tool you see
fit but make sure to explain your representation if you do not use a
Gantt chart (seen in class, but here is the Wikipedia article
about <a href="https://en.wikipedia.org/wiki/Gantt_chart">Gantt
Chart</a>) or ASCII art.
<p/>

Reminder: ASCII art, e.g., running the task "T1 4 c4 i3 c2" on the CPU
(while there is no other task to run) is a representation like:

<pre>
      0           1           2           3
Time  01234 56789 01234 56789 01234 56789 0...
 CPU  ----1 111-- -11-- ----- ----- ----- -
  T1  ----R RRRII IRR-- ----- ----- ----- -
  
where -: CPU (resource) not unused
      #: Task T# is running on the CPU (resource)
  and R: Task is Running
      I: Task is Waiting for I/O
      E: Task is Ready
      -: Task has not started or is terminated
</pre>

Assume that there is a second task to be run defined by "T2 5 c4" with a
Non-Preemptive FCFS, the ASCII art would look like:

<pre>
      0           1           2           3
Time  01234 56789 01234 56789 01234 56789 0...
 CPU  ----1 111-- -1122 2---- ----- ----- -
  T1  ----R RRRII IRR-- ----- ----- ----- -
  T2  ----- EEEEE EEERR RR--- ----- ----- -

  T2 has to wait for the completion of T1 before being dispatched
</pre>

With a Preemptive FCFS:

<pre>
      0           1           2           3
Time  01234 56789 01234 56789 01234 56789 0...
 CPU  ----1 11122 2211- ----- ----- ----- -
  T1  ----R RRRII IERR- ----- ----- ----- -
  T2  ----- EEERR RR--- ----- ----- ----- -

  T2 "jumps in" when T1 waits for IO (and then T1 has to wait for T2 completion or CPU idleness)
</pre>

<h2>Problem</h2>

Three tasks are specified in the following table:

<table>
  <tr>
    <th>Task</th><th>Arrival Date</th><th>Bursts</th>
  </tr>
  <tr>
    <td>T1</td><td>0</td><td>c4 i8 c4 i8 c4 i8 c4</td>
  </tr>
  <tr>
    <td>T2</td><td>2</td><td>c12 i2 c12 i2 c2</td>
  </tr>
  <tr>
    <td>T3</td><td>3</td><td>c6 i6 c6 i6 c3</td>
  </tr>
</table>

The tasks run on one CPU (which has only one core).

<h3>Question 1 [8 points total]</h3> 

Ignore context-switching for the moment (assume that they are
instantaneous and that you can have, e.g. --112 2211- in the previous
diagram).<p/>

All I/Os are supposed to take exactly the time which is described. If
you see i6, think of it as if the task was idle during 6 time units.
and then is made Ready ("may resume" and not "resumes" since it depends
on the scheduling policy)

<h4>Question 1.1 [4 points]</h4> 

Show how the activity diagram for the different tasks running on the
CPU for each of the following schedulers:

<ul>
  <li>Non-Preemptive FCFS</li>
  <li>Preemptive SJF (Evaluate the task duration in terms of
  CPU-Bursts. Events that cause context-switching: New Task Arrival,
  IO, Task Completion)</li>
  <li>RR with a quantum of 5</li>
  <li>MLFQ consisting of 2 queues: 1 high-priority queue (RR with
    quantum = 8) and 1 low-priority queue (RR with time-quantum =
    4). A new task has high-priority and is put at the back of the
    queue. Priority is decreased only if the whole time quantum is
    used, or remains the same otherwise. The scheduler runs the same
    loop over and over: it runs jobs from the high-priority queue,
    then jobs from the low-priority queues, and finally updates
    priorities</li>
</ul>

<h4>Question 1.2 [4 points]</h4> 

Evaluate (don't forget to detail) the following metrics:

<ul>
  <li>CPU Utilization Percentage</li>
  <li>Average Turnaround Time</li>
  <li>Average Response Time</li>
</ul>

for each of the algorithms of question 1.1. Display the results in a
table.

Comment each result (which scheduling policy seems to be good
in terms of the different metrics, was it expected...)

<h3>Question 2 [8 points total]</h3> 

A context-switch+picking decision activity takes 1 time unit and the
system is completely blocked when it happens (not true in reality but
we need to keep it simple). When no task is running, the context-switch
has no cost.

If you use ASCII art to represent the activity diagram, use a symbol
which has not been used already (like 'o' for instance)

I/Os still happen "in parallel" (no need to show an entry for the IOs)

For instance, with the two tasks "T1 1 c3 i10 c10" and "T2 3 c2 i5 c6"

<pre>
     0           1           2           3
Time 01234 56789 01234 56789 01234 56789 0...
 CPU -111o 22--1 1-222 2o1...
  T1 -RRRI IIIII IIIIE EoR...
  T2 ---EE RRIII IIRRR RoE...
         |- I/O induced context-switch (at t=4)
            |- |-  |- "Free" context-switches (at t=6, t=9, t=12)
                        |- Scheduling timeout context-switch here (at t=16)
</pre>

<h4>Question 2.1 [4 points]</h4> 

Show the activity diagrams taking into account the context-switches
for the scheduling algorithms mentioned in question 1.1.

Hint: The diagrams should be derived from the question 1.


<h4>Question 2.2 [4 points]</h4> 

Evaluate the same metrics of question 1.2. Evaluate the overhead of
context-switches (in time units and percentage) for all schedulers and
add them to the table.

<h3>Question 3 [8 points total]</h3> 

A context-switches+picking decision takes 1 time unit.

I/O are made to the same serial I/O-unit (equivalent to a
non-preemptive FCFS). This means that if, say, T2 tries to use I/O
while T1 is using it, T2 is blocked (symbol: B) until T1 I/O
completion.

For instance, with the two tasks "T1 1 c3 i10 c10" and "T2 3 c2 i5 c6"
the activity diagram should look like:

<pre>
     0           1           2           3
Time 01234 56789 01234 56789 01234 56789 0...
 CPU -111o 22--- ----1 11111 o22...
  IO ----1 11111 11112 2222- ------
  T1 -RRRI IIIII IIIIR RRRRR oEE...
  T2 ---Eo RRBBB BBBBI IIIIE oRR...
         |- I/O induced Context-Switch
                             |- Scheduler Context-Switch Arbitrary Decision
</pre>

<h4>Question 3.1 [4 points]</h4> 

Show the activity diagram taking into account the context-switches for
the scheduling algorithms mentioned in question 2.1.

<h4>Question 3.2 [4 points]</h4> 

Evaluate the same metrics of question 2.2. Evaluate the overhead of
context-switches (in time units and percentage) for all schedulers and
add them to the table.

<h3>Question 4 [2 points total]</h3>

Compare and discuss the results of the algorithms for the
questions 2 and 3.

