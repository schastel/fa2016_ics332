---
morea_id: lecturenotes_scheduling
morea_type: reading
title: "Scheduling"
published: True
morea_summary: "Scheduling definitions, standard algorithms, what OSes do"
morea_url: /morea/060_Scheduling/ics332_scheduling.pdf
morea_labels: 
  - "Lecture notes"
---
