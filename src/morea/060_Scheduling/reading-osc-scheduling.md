---
morea_id: osc_scheduling
morea_type: reading
title: "OSC Reading"
published: True
morea_summary: "Sections 3.2, 6.1-3, 6.7-9"
morea_url: "javascript:void(0);"
morea_labels: 
  - "Reading"
---
