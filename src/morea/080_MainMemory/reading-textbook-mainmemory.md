---
morea_id: textbook_mainmemory
morea_type: reading
title: "Main Memory"
published: True
morea_labels: 
  - "Reading"
morea_summary: "Textbooks readings"
---

OSC: Sections 8.1, 8.2, and 8.3

OSTEP: 

<a target="_blank" href="http://pages.cs.wisc.edu/~remzi/OSTEP/vm-api.pdf">Chapter 14 - Memory API</a>

<a target="_blank" href="http://pages.cs.wisc.edu/~remzi/OSTEP/vm-mechanism.pdf">Chapter 15 - Address Translation</a>
