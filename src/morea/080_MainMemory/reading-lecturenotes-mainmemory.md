---
morea_id: lecturenotes_mainmemory
morea_type: reading
title: "Main Memory"
published: True
morea_summary: "Contiguous allocation algorithms, binpacking algorithms, fragmentation"
morea_url: /morea/080_MainMemory/ics332_mainmemory.pdf
morea_labels: 
  - "Lecture notes"
---
