---
morea_id: outcomememory
morea_type: outcome
title: "Main Memory"
published: True
morea_sort_order: 60
---

- Be proficient with the needed discrete math skills to reason quantitatively about memory (addressing, counting)
- Understand the memory management problem 
- Know standard bin packing algorithm for solving the "contiguous memory" management problem


