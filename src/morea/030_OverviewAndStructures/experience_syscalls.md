---
morea_id: experience_structure
morea_type: experience
title: "Homework Assignment #3"
published: True
morea_summary: "An assignment in which you use strace to count system calls and spy on programs"
morea_labels: 
  - "Due Thu Sept. 29th 2016 23:55 HST"

---

## HW#3: Hands-On Assignment -- System Calls

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

Check the 
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a> 
for the late assignment policy for this course.

<b>This assignement gives a total of 19 points and 2 extra-credits.</b>

#### What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw3_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw3_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw3_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw3_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw3_YourUHUserName directory:

 * index.html: your report
 * ex1/trace_c (see exercise 1)
 * ex1/trace_java (see exercise 1)
 * ex1/trace_bash (see exercise 1)

Additional content can be provided if necessary

---

#### Environment

For this assignment you need a Linux Virtual Machine
(see <a href="{{ site.baseurl }}/morea/010_GettingStarted/experiences-gettingstarted.html">Assignment #1</a>). 

---

### Exercise #1: "Hello World!" [12 pts]

In this exercise you will use <tt><b>strace</b></tt> to investigate
the system calls placed when running HelloWorld programs in C, in
Java, and in bash. Each of them has a unique purpose, display:
<pre>
    Hello, World! It's &lt;current time&gt;!
</pre>

The source code of each program can be found in:

 * [HelloWorld.c](HW/HelloWorld.c)
 * [HelloWorld.java](HW/HelloWorld.java)
 * [HelloWorld.sh](HW/HelloWorld.sh)

#### C implementation

You can create an executable (named HelloWorld) by
running:
<pre>
gcc -o HelloWorld HelloWorld.c
</pre>

and execute it this way:

<pre>
./HelloWorld
</pre>

#### Java implementation

You will compile the source code by running:

<pre>
javac HelloWorld.java
</pre>

and execute it this way:

<pre>
java HelloWorld
</pre>

#### Bash implementation

Just execute the script by running:

<pre>
bash HelloWorld.sh
</pre>

#### Counting (6 points)

Using <tt>strace</tt>, count how many <b>different</b> system calls
are placed by the program built from the C implementation. Explain how
you get that number and place the associated strace report in your
report under the name ex1/trace_c.

Same activity for the Java implementation, the name of the strace
report being ex1/trace_java.

Same activity for the bash implementation, the name of the strace
report being ex1/trace_bash.

Show the results in one table (where the system calls are sorted
alphabetically) looking like this one:

<table border="1">
  <tr>
    <th>System Call</th>
    <th>C</th>
    <th>Java</th>
    <th>bash</th>
  </tr>
  <tr>
    <td>aaaa</td>
    <td>-</td>
    <td>4</td>
    <td>2</td>
  </tr>
  <tr>
    <td>bbbb</td>
    <td>75</td>
    <td>14</td>
    <td>-</td>
  </tr>
  <tr><td>...</td><td>...</td><td>...</td></tr>
</table>

Download the <a href="HW/table.html" download>corresponding HTML
source code</a> for the table if you are not at ease with HTML.

Hint 1: Read the strace manpage to know (1) how to write the strace report
to an output file; (2) how to get a summary report.

Hint 2: The manpages contents can be searched.

#### Classification (6 points)

Pick 6 system calls from the list of system calls that you found in
the previous question.

Quickly identify from their manpage to which category of system
calls the 6 system calls you picked could belong to (Briefly explain why):

 * Process Management
 * Memory Management
 * I/O
 * Protection / Security
 * Other

Note: Make sure that you are reading the correct manpage (e.g. <tt>man
2 write</tt> and not <tt>man write</tt>).

Side note: Take this question at the end of the semester: Measure how
much your knowledge about OS has progressed <tt>:)</tt>

---

### Date and Privileges (7 points + 2 extra-credits)

You will need <tt>strace</tt> again in this question

Prelude: You will need a clone of the Linux Virtual Machine that you
installed for HW#1. See the Interlude for the cloning details.

#### Question 1 (1 pt)

 According to its documentation, in Linux, what are the two functions
of the <tt>date</tt> command?

#### Question 2 (2 pts)

 Execute the date command in the "get date" mode (by running just
"date")?  Run it through the strace tool. What are the system calls
responsible for actually getting the system date/time? Show the
corresponding lines of the strace report in your HTML report.

#### Question 3 (2 pts)

Execute the date command in the "set date" mode
(by running e.g.  "date -s 2012-01-01")? What can you observe? Explain
what happens. How is it visible in the strace report? Show the
corresponding lines of the strace report in your HTML report.

#### Interlude 

Clone your Virtual Machine. Name the clone by "Compromised!
Delete after use! How come it's not been wiped out yet!" or something
like that (Do you get the idea?)

#### Question 4 (2 pts)

 In your <b>cloned</b> Linux Virtual Machine, force the execution of
date in the "set date" mode as superuser. What are the system calls
that are involved that give you access to the privileged mode?

#### Question 5 (Extra-credit: 2 pts)

WARNING! What we do in this question risks to seriously compromise
your system. Do the following only on a virtual machine that you will
delete afterwards! Do not do it on your regular computer (MacOS X
Users I'm staring at you!). YOU'VE BEEN WARNED!

Copy the date program under a different name

  sudo cp /bin/date /bin/dangerous_date

Set its SUID bit (more about that on the lecture on Files Systems)

  sudo chmod +s /bin/dangerous_date

Now execute the date command of the "set date" mode again
(e.g. dangerous_date -s "2015-10-10")"? What do you observe? Why
(looking at the documentation of chmod should help). Why is it
dangerous?

#### Postlude

Shut down the "Compromised!  Delete after use! How come it's not been
wiped out yet!" Virtual Machine and delete it.

#### Moral/Conclusion

If one day, a random piece of software asks to get administrative
rights, it better has very good reasons to do so.
