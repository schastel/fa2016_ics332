---
morea_id: lecturenotes_overview
morea_type: reading
title: "OS Overview"
published: True
morea_summary: "The big picture, the kernel, kernel- vs. user-mode"
morea_url: /morea/030_OverviewAndStructures/os_overview.pdf
morea_labels: 
  - "Lecture notes"
---
