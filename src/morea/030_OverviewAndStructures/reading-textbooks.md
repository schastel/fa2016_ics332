---
morea_id: textbook_overview_and_structures
morea_type: reading
title: "Textbooks Reading"
published: True
morea_summary: "Textbooks chapters relevant for OS Overview and Structures"
morea_labels: 
  - "Reading"
---

- OSC Chapter 1 is related to OS Overview (make sure to read 1.11 and 1.12)

- OSC Chapter 2 is related to OS Structures

- The [OSTEP Introduction](http://pages.cs.wisc.edu/~remzi/OSTEP/intro.pdf) is relevant for both
OS Overview and OS Structures.
